import React, { useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import CheckBox from "expo-checkbox";

const CheckDays = ({ selectedDays, setSchedule }) => {
  const onToggleDay = (day) => {
    if (selectedDays.includes(day)) {
      // Se o dia já estiver selecionado, remova-o do array
      setSchedule((prevState) => ({
        ...prevState,
        days: prevState.days.filter((item) => item !== day),
      }));
    } else {
      // Se o dia não estiver selecionado, adicione-o ao array
      setSchedule((prevState) => ({
        ...prevState,
        days: [...prevState.days, day],
      }));
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Selecione os dias:</Text>
      <View style={styles.checkboxContainer}>
        <CheckBox
          value={selectedDays.includes("Seg")}
          onValueChange={() => onToggleDay("Seg")}
          style={styles.checkbox}
        />
        <Text style={styles.checkboxLabel}>Segunda</Text>
      </View>
      <View style={styles.checkboxContainer}>
        <CheckBox
          value={selectedDays.includes("Ter")}
          onValueChange={() => onToggleDay("Ter")}
          style={styles.checkbox}
        />
        <Text style={styles.checkboxLabel}>Terça</Text>
      </View>
      <View style={styles.checkboxContainer}>
        <CheckBox
          value={selectedDays.includes("Qua")}
          onValueChange={() => onToggleDay("Qua")}
          style={styles.checkbox}
        />
        <Text style={styles.checkboxLabel}>Quarta</Text>
      </View>
      <View style={styles.checkboxContainer}>
        <CheckBox
          value={selectedDays.includes("Qui")}
          onValueChange={() => onToggleDay("Qui")}
          style={styles.checkbox}
        />
        <Text style={styles.checkboxLabel}>Quinta</Text>
      </View>
      <View style={styles.checkboxContainer}>
        <CheckBox
          value={selectedDays.includes("Sex")}
          onValueChange={() => onToggleDay("Sex")}
          style={styles.checkbox}
        />
        <Text style={styles.checkboxLabel}>Sexta</Text>
      </View>
      <View style={styles.checkboxContainer}>
        <CheckBox
          value={selectedDays.includes("Sab")}
          onValueChange={() => onToggleDay("Sab")}
          style={styles.checkbox}
        />
        <Text style={styles.checkboxLabel}>Sabado</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
        borderColor: "#202022",
        borderRadius: 5,
        padding: 10,
        backgroundColor: "#FFF", // Light background for better contrast
        width: '85%'
      },
      label: {
        fontSize: 16,
        marginBottom: 5,
        color: "#202022",
        fontFamily: "PoppinsBold", // Assuming Poppins font family is available
      },
      checkboxContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginBottom: 10,
      },
      checkbox: {
        marginRight: 10, // Add margin for spacing
        color: "#202022", // Adjust checkbox color if needed (based on default styling)
      },
      checkboxLabel: {
        fontSize: 16,
        color: "#202022",
        fontFamily: "PoppinsRegular", // Assuming Poppins font family is available
      },
});

export default CheckDays;