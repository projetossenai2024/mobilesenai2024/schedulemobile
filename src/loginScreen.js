import React, { useState } from "react";
import { View, Button, TextInput, StyleSheet, Text, Alert, Platform, Pressable } from "react-native";
import { useNavigation } from "@react-navigation/native";
import api from "./axios/axios";

const HomeScreen = () => {
  const navigation = useNavigation();
  const [credentials, setCredentials] = useState({ email: "", password: "" });

  const handleLogin = () => {
    // Exemplo de chamada para login
    api.loginUser(credentials)
      .then(response => {
        console.log(response.data); // Exibe a resposta da API no console
        Alert.alert("Bem vindo", response.data.message)
        navigation.navigate("HomePage", {
          user: '12345680091',//response.data.user.cpf,
          nameUser: 'Igor'//response.data.user.name,
        }); // Vá para a próxima tela após Login
      })
      .catch(error => {
        Alert.alert("Erro", error.response.data.error);
      });
  };

  const handleSignupRedirect = () => {
    // Alert.alert("OBS", "Achou que eu iria te entregar tudo pronto?");
    navigation.navigate('Signup')
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Login</Text>
      <View style={styles.formContainer}>
        <TextInput
          style={styles.input}
          placeholder="Email"
          placeholderTextColor="#202020"
          onChangeText={(text) => setCredentials({ ...credentials, email: text })}
          value={credentials.email}
        />
        <TextInput
          style={styles.input}
          placeholder="Password"
          placeholderTextColor="#202020"
          onChangeText={(text) =>
            setCredentials({ ...credentials, password: text })
          }
          value={credentials.password}
          secureTextEntry
        />
        {/* <Button title="Login" onPress={handleLogin} style={styles.button}/>
        <Button
          title="Cadastre-se"
          onPress={handleSignupRedirect}
          style={styles.button}
        /> */}

        <View style={styles.buttonContainer}>
          <Pressable onPress={handleLogin} style={styles.button}>
            <Text style={styles.buttonText}>Login</Text>
          </Pressable>
          <Pressable onPress={handleSignupRedirect} style={styles.button}>
            <Text style={styles.buttonText}>Cadastre-se</Text>
          </Pressable>
        </View>

      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#AED3F2',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 38,
    fontFamily: 'PoppinsBold',
    marginBottom: 20,
    color: '#202022', // Cor de título mais escura
  },
  formContainer: {
    width: '100%',
    backgroundColor: '#F2F2F2',
    padding: 25,
    borderRadius: 10,
    marginBottom: 15,
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      }
    })
  },
  input: {
    width: '100%',
    height: 55,
    backgroundColor: '#FFF',
    borderWidth: 0,
    marginBottom: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    fontFamily: 'PoppinsLight',
    color: '#202022'
  },
  buttonContainer:{
      flexDirection: 'row',
      justifyContent: 'space-around'
  },
  button: {
    backgroundColor: '#4586BF',
    borderRadius: 8,
    padding: 10,
    width: '45%',
    marginTop: 10,
    alignSelf: 'center',
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      },
    }),
  },
  buttonText: {
    color: '#fff',
    fontSize: 18,
    fontFamily: 'PoppinsMedium',
    alignSelf: 'center',
  },
});

export default HomeScreen;
