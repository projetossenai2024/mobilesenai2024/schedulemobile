import React, { useState } from "react";
import { View, Button, TextInput, StyleSheet, Text, Alert, Platform, Pressable } from "react-native";
import { useNavigation } from "@react-navigation/native";
import api from "./axios/axios";

const SignupScreen = () => {
    const navigation = useNavigation();
    const [credentials, setCredentials] = useState({ email: "", password: "", name: '', cpf: '' });

    const handleSignup = () => {
        // Exemplo de chamada para login
        api.createUser(credentials)
            .then(response => {
                console.log(response.data); // Exibe a resposta da API no console
                Alert.alert("Bem vindo", response.data.message)
                navigation.navigate("HomePage"); // Vá para a próxima tela após Login
            })
            .catch(error => {
                Alert.alert("Erro", error.response.data.error);
            });
    };

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Signup</Text>
            <View style={styles.formContainer}>
                <TextInput
                    style={styles.input}
                    placeholder="Name"
                    placeholderTextColor="#202020"
                    onChangeText={(text) => setCredentials({ ...credentials, name: text })}
                    value={credentials.name}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Email"
                    placeholderTextColor="#202020"
                    onChangeText={(text) => setCredentials({ ...credentials, email: text })}
                    value={credentials.email}
                />
                <TextInput
                    style={styles.input}
                    placeholder="CPF"
                    placeholderTextColor="#202020"
                    onChangeText={(text) => setCredentials({ ...credentials, cpf: text })}
                    value={credentials.cpf}
                    keyboardType="numeric"
                />
                <TextInput
                    style={styles.input}
                    placeholder="Password"
                    placeholderTextColor="#202020"
                    onChangeText={(text) =>
                        setCredentials({ ...credentials, password: text })
                    }
                    value={credentials.password}
                    secureTextEntry
                />

                <Pressable onPress={handleSignup} style={styles.button}>
                    <Text style={styles.buttonText}>Cadastre-Se</Text>
                </Pressable>


            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#AED3F2',
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    title: {
        fontSize: 38,
        fontFamily: 'PoppinsBold',
        marginBottom: 20,
        color: '#202022', // Cor de título mais escura
    },
    formContainer: {
        width: '100%',
        backgroundColor: '#F2F2F2',
        padding: 25,
        borderRadius: 10,
        marginBottom: 15,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.2,
                shadowRadius: 3,
            }
        })
    },
    input: {
        width: '100%',
        height: 55,
        backgroundColor: '#FFF',
        borderWidth: 0,
        marginBottom: 10,
        paddingHorizontal: 10,
        borderRadius: 10,
        fontFamily: 'PoppinsLight',
        color: '#202022'
    },

    button: {
        backgroundColor: '#4586BF',
        borderRadius: 8,
        padding: 10,
        width: '80%',
        marginTop: 10,
        alignSelf: 'center',
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.2,
                shadowRadius: 3,
            },
        }),
    },
    buttonText: {
        color: '#fff',
        fontSize: 20,
        fontFamily: 'PoppinsMedium',
        alignSelf: 'center',
    },
});

export default SignupScreen;
