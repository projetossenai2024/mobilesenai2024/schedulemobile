import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "./src/loginScreen";
import HomePage from "./src/homePage";
import SignupScreen from "./src/signupScreen";
import { useFonts } from 'expo-font';


const Stack = createStackNavigator();

export default function App() {

    // Carrega as fontes externas instaladas
    const [fontsLoaded] = useFonts({
      PoppinsBlack: require('./assets/fonts/Poppins/Poppins-Black.ttf'),
      PoppinsBold: require('./assets/fonts/Poppins/Poppins-Bold.ttf'),
      PoppinsSemiBold: require('./assets/fonts/Poppins/Poppins-SemiBold.ttf'),
      PoppinsRegular: require('./assets/fonts/Poppins/Poppins-Regular.ttf'),
      PoppinsMedium: require('./assets/fonts/Poppins/Poppins-Medium.ttf'),
      PoppinsLight: require('./assets/fonts/Poppins/Poppins-Light.ttf'),
  
    });
  
    // Verifica se todas as fontes foram carregadas antes de renderizar o conteúdo
    if (!fontsLoaded) {
      return null; // Retorna o tipo null como indicador de carregamento
    }
  

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ title: "Login" }}
        />
        <Stack.Screen
          name="Signup"
          component={SignupScreen}
          options={{ title: "Signup" }}
        />
        <Stack.Screen
          name="HomePage"
          component={HomePage}
          options={{ title: "Classroom Reservation" }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
